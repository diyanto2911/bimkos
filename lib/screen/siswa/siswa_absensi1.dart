import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smk_losarangg/screen/login.dart';
import 'package:smk_losarangg/screen/siswa/siswa_mulaibimbingan.dart';
import 'package:smk_losarangg/screen/siswa/siswa_pemberitahuan.dart';
import 'package:smk_losarangg/screen/siswa/siswa_profil.dart';
import 'package:smk_losarangg/screen/tentang.dart';

class Absensi1 extends StatefulWidget {
  @override
  _Absensi1State createState() => _Absensi1State();
}

class _Absensi1State extends State<Absensi1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tahun Ajaran: -----"),
          backgroundColor: Color(0xFFFFE75E),
        ),
        body: Stack(
          children: <Widget>[
            
            Positioned(
                top: 30,
                left: 10,
                right: 10,
                bottom: 10,
                child: GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 0.7,
                  padding: const EdgeInsets.all(4.0),
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MulaiBimbingan()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/14.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Juli",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/3.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Agustus",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MulaiBimbingan()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/14.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "September",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/3.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Oktober",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MulaiBimbingan()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/14.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "November",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/3.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Desember",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MulaiBimbingan()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/14.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Januari",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/3.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Februari",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MulaiBimbingan()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/14.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Maret",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/3.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "April",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MulaiBimbingan()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/14.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Mei",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profil()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/3.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Juni",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),

                  ],
                ))
          ],
        ));
  }
}
