import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smk_losarangg/screen/login.dart';
import 'package:smk_losarangg/screen/siswa/siswa_absensi1.dart';
import 'package:smk_losarangg/screen/siswa/siswa_home.dart';
import 'package:smk_losarangg/screen/siswa/siswa_mulaibimbingan.dart';
import 'package:smk_losarangg/screen/siswa/siswa_pemberitahuan.dart';
import 'package:smk_losarangg/screen/siswa/siswa_profil.dart';
import 'package:smk_losarangg/screen/siswa/siswa_riwayatbimbingan.dart';
import 'package:smk_losarangg/screen/tentang.dart';
class Absensi extends StatefulWidget {
  @override
  _AbsensiState createState() => _AbsensiState();
}

class _AbsensiState extends State<Absensi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Absensi"),
          backgroundColor: Color(0xFFFFE75E),
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
                top: 30,
                left: 10,
                right: 10,
                bottom: 10,
                child: ListView(
                  children: <Widget>[
                    
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Absensi1()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/8.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Tahun Ajaran\n2018-2019",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Absensi1()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/7.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Tahun Ajaran\n2019-2020",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Absensi1()));
                      },
                      child: Card(
                        elevation: 3,
                        color: Color(0xFFFFE75E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                "assets/7.png",
                                height: 160,
                                width: 300,
                              ),
                            ),
                            Text(
                              "Tahun Ajaran\n2020-2021",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.assistant(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF527318)),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ));
  }
}
