import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:google_fonts/google_fonts.dart';

class Tentang extends StatefulWidget {
  Tentang({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _Tentang createState() => _Tentang();
}

class _Tentang extends State<Tentang> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Stack(
      
       children: <Widget>[
         Padding(
           padding: const EdgeInsets.only(top: 250),
           child: Image.asset('assets/logo.png',),
         ),
        //  SizedBox.expand(
        //    child: Image.asset("assets/logo.png", height: 450, width: 450, fit: BoxFit.cover),
        //  ),
        DraggableScrollableSheet(
          minChildSize: 0.1,
          initialChildSize: 0.22,
          builder: (context, scrollControler){
            return SingleChildScrollView(
              controller: scrollControler,
              child: Container(
                constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 32, right: 32, top: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(
                            height: 100,
                            width: 100,
                            child: ClipOval(
                              child: Image.asset('assets/logo.png', fit: BoxFit.cover),
                            ),
                          ),
                          SizedBox(width: 16,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Tentang Aplikasi", style: TextStyle(color: Colors.grey[800],
                                fontSize: 36, fontWeight: FontWeight.bold),)
                              ],
                            ),
                            
                          ),
                          
                        ],
                      ),
                      
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30, left: 35, right: 35),
                      child: Column(
                        children: <Widget>[
                          Text("Ini merupakan aplikasi bimbingan dan konseling dari SMK Negeri 1 Losarang. Aplikasi ini dapat diakses oleh seluruh siswa SMK Negeri 1 Losarang yang telah memiliki username serta password.\nAplikasi ini digunakan untuk melakukan bimbingan dan konseling diantara guru BK dengan siswa.\n terus apalagi tar mikir dulu",
                          style: GoogleFonts.aBeeZee(color: Colors.grey, fontSize: 20, ), textAlign: TextAlign.justify,)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
       ],
      ),
      
    );
  }
}
