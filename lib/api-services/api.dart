class ApiServer{

  //local server
  static final String baseUrl="http://192.168.43.27:8000";
  static final String login="$baseUrl/login";
  static final String getBimbingan="$baseUrl/get_bimbingan";
  static final String getBimbinganGuru="$baseUrl/get_bimbingan_guru";
  static final String tambahBimbingan="$baseUrl/tambah_bimbingan";
  static final String updateStatusBimbingan="$baseUrl/update_status_bimbingan";
  static final String getGuruBK="$baseUrl/get_guru_bk";
  static final String updateStatus="$baseUrl/update_status";
  static final String updateStatusSiswa="$baseUrl/get_status_siswa";
  static final String listChat="$baseUrl/list_chat";
  static final String sendChat="$baseUrl/send_chat";
}