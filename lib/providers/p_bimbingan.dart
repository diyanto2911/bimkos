import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:ndialog/ndialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smk_losarangg/api-services/api.dart';
import 'package:smk_losarangg/models/m_list_bimbingan.dart';
import 'package:smk_losarangg/models/m_list_bimbingan_guru.dart';
import 'package:smk_losarangg/models/m_list_chat.dart';
import 'package:smk_losarangg/providers/p_users.dart';
import 'package:smk_losarangg/screen/chat_screen.dart';
import 'package:smk_losarangg/screen/siswa/v_chat_room.dart';

class ProviderBimbingan extends ChangeNotifier {
  Dio dio;

  bool _loadingbimbingan = true;
  int _codeAPibim;
  String _messageApibim;

  ModelBimbingan _modelBimbingan;

  bool get loadingbimbingan => _loadingbimbingan;

  set loadingbimbingan(bool value) {
    _loadingbimbingan = value;
  }

  int get codeAPibim => _codeAPibim;

  set codeAPibim(int value) {
    _codeAPibim = value;
  }

  String get messageApibim => _messageApibim;

  set messageApibim(String value) {
    _messageApibim = value;
    notifyListeners();
  }

  ModelBimbingan get modelBimbingan => _modelBimbingan;

  set modelBimbingan(ModelBimbingan value) {
    _modelBimbingan = value;
    notifyListeners();
  }

  Future<ModelBimbingan> getListBimbingan() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var nis = pref.get("nis");
    String url = "${ApiServer.getBimbingan}/$nis";
    Response response;
    dio = new Dio();
    response = await dio.get(url);
    if (response.statusCode == 200) {
      loadingbimbingan = false;
      modelBimbingan = ModelBimbingan.fromJson(response.data);
    }
    return modelBimbingan;
  }

  ModelBimbinganGuru _modelBimbinganGuru;


  ModelBimbinganGuru get modelBimbinganGuru => _modelBimbinganGuru;

  set modelBimbinganGuru(ModelBimbinganGuru value) {
    _modelBimbinganGuru = value;
    notifyListeners();
  }

  Future<ModelBimbinganGuru> getListBimbinganGuru() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var nis = pref.get("id_sekolah");
    String url = "${ApiServer.getBimbinganGuru}/$nis";
    Response response;
    dio = new Dio();
    response = await dio.get(url);
    if (response.statusCode == 200) {
      loadingbimbingan = false;
      modelBimbinganGuru = ModelBimbinganGuru.fromJson(response.data);
    }
    return modelBimbinganGuru;
  }

  Future<bool> tambahBimbingan(
      {BuildContext context, String subject, String isiBimbingan}) async {
    bool result = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var nis = prefs.get("nis");
    var idSekolah = prefs.get("id_sekolah");
    var nik;
    Provider.of<ProviderUsers>(context,listen: false).getGuruBK().then((value) {
      nik=value.data[0].nik;
    });

    ProgressDialog pg = new ProgressDialog(context, message: Text("Loading"));
    pg.show();
    initializeDateFormatting('id-ID');
var tgl=DateFormat('yyyy-MM-dd', 'id-ID').format(DateTime.now());
print(tgl);
    dio = new Dio();
    Response response;
    response = await dio.post(ApiServer.tambahBimbingan, data: {
      "nis": "$nis",
      "subject": "$subject",
      "isi_bim": "$isiBimbingan",
      "tgl_bim":
          "$tgl",
      "id_sekolah"  : "$idSekolah",
      "timestamps": "${DateTime.now().millisecondsSinceEpoch.toString()}"
    });
    print(response.data);
    if (response.statusCode == 200) {
      pg.dismiss();
    if(response.data['code']==200){
      Fluttertoast.showToast(msg: "Data Bimbingan Berhasil dibuat",
      backgroundColor: Colors.green,
      gravity: ToastGravity.TOP,
      toastLength: Toast.LENGTH_SHORT,
      textColor: Colors.white);
      Provider.of<ProviderBimbingan>(context,listen: false).getListBimbingan().then((value) {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ViewChatRoom(
          isiBimbingan: isiBimbingan,
          idBimbigan: response.data['data'].toString(),
          peerId: nik,
          peerAvatar: nik,
        )));
      });

    }else{
      Fluttertoast.showToast(msg: "Data Bimbingan gagal dibuat",
          backgroundColor: Colors.red,
          gravity: ToastGravity.TOP,
          toastLength: Toast.LENGTH_SHORT,
          textColor: Colors.white);
      Navigator.pop(context);
    }

    }

    return result;
  }

  Future<bool> updateStatusBimbingan(
      {BuildContext context, String idBimbingan,bool isSiswa=false}) async {
    bool result = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var nis = prefs.get("nis");


    initializeDateFormatting('id-ID');
    var tgl=DateFormat('yyyy-MM-dd', 'id-ID').format(DateTime.now());
    print(tgl);
    dio = new Dio();
    Response response;
    String url="${ApiServer.updateStatusBimbingan}/$idBimbingan";
    if(isSiswa==false){
      response = await dio.post(url);
    }else{
      response = await dio.post(url,queryParameters: {
        "isSiswa" : "true"
      });
    }
    print(response.data);


    return result;
  }

  ModelListChat _modelListChat;

  ModelListChat get modelListChat => _modelListChat;

  set modelListChat(ModelListChat value) {
    _modelListChat = value;
    notifyListeners();
  }

  Future<ModelListChat> getListChat({String nikGuru,String nis,bool guru=false,String idBimbingan})async{
    dio=new Dio();
    SharedPreferences pref=await SharedPreferences.getInstance();



      Response response;
      response=await dio.get(ApiServer.listChat,queryParameters: {
        "id_bimbingan" : "$idBimbingan"
      });


      if(response.statusCode==200){
        modelListChat = ModelListChat.fromJson(response.data);

      }

      return modelListChat;

  }


  Future<bool> sendChat({String idBimbigan,String nik,String nis,String content,String senderId})async{

    String url="${ApiServer.sendChat}/$idBimbigan";

    dio =new Dio();
    Response response;
    response=await dio.post(url,data: {
      "nik" : "$nik",
      "nis" : "$nis",
      "content" : "$content",
      "sender_id" : "$senderId"
    });

    if(response.statusCode==200){

    }

    return true;


  }
}
